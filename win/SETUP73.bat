@echo off
rem !/bin/csh
rem SETUP73 version 7.1 AJB 7/2009.  Tranlsated for Windows command line window by RJ Bauer 7/2009 - mods for PDx-Pop 5 added 3/30/2011
echo NONMEM 7.3.0 INSTALLATION UTILITY - Windows/DOS batch file version
echo For instructions, see readme_7.3.0.pdf or readme_7.3.0.txt on the CD
rem echo For instructions, see Appendix 4 of
rem echo NONMEM Users Guide - Part III (NONMEM Installation Guide)
echo.
set defcdd=d:
set defh=c:\nm73
set deff=ifort
set defo=y
set defar=link
set deffilepath=
set defs=same
set defr=rec
set defi=i
set defzf=nonmem73e.zip
set defzu=nonmem73r.zip
set defu=unzip.exe

set cdd=%defcdd%
set h=%defh%
set f=%deff%
set o=%defo%
set ar=%defar%
set filepath=%deffilepath%
set s=%defs%
set r=%defr%
set i=%defi%
set zf=%defzf%
set zu=%defzu%
set u=%defu%
set cddrv=d:
set hdrv=c:

set comline=%*
if "%1" == "help" goto def
if "%1" == "" goto p0
set cdd=%1
set cddrv=%~d1
for %%x in (a b c d e f g h i j k l m n o p q r s t u v w x y z) do if "%cdd%"=="%%x" (set cdd=%cdd%:&& set cddrv=%cdd%:)
for %%x in (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) do if "%cdd%"=="%%x" (set cdd=%cdd%:&& set cddrv=%cdd%:)
if not "%2" == "" set h=%2
if not "%2" == "" set hdrv=%~d2
if not "%3" == "" set f=%3
if "%f%"=="gfortran" set ar=ar
if "%f%"=="g95" set ar=ar
if not "%4" == "" set o=%4
if not "%5" == "" set ar=%5
if not "%6" == "" set s=%6
if not "%7" == "" set r=%7
if not "%8" == "" set i=%8
rem next args are not generally known or needed
if not "%9" == "" set u=%9
shift
if not "%9" == "" set zf=%9
shift
if not "%9" == "" set zu=%9
goto p0

:def
 echo Usage: SETUP73 cd h f o ar s r i u zf zu
 echo       where cd = source path (e.g. d:\)
 echo             h = name of NONMEM 7.3.0 directory
 echo             f = command for FORTRAN compiles
 echo             o = optimization (y / n)
 echo            ar = full path name of lib/ar command
 echo             s = same (SIZES.f90)
 echo             r = norec (no recompile) / rec (recompile)
 echo             i = i (interactive) / q (non-interactive)
 echo             u = unzip program
 echo             zf = encrypted source files
 echo             zu = unencrypted files
 echo Default: SETUP73 %defcdd% %defh% %deff% %defo% %defar% %defs% %defr% %defi% %defu% %defzf% %defzu%
 goto lexit

:p0
echo SETUP73 %cdd% %h% %f% %o% %ar% %s% %r% %i% %u% %zf% %zu%
echo CD-ROM drive is %cdd%
echo NONMEM 7.3.0 directory is %h%
echo Command for FORTRAN compiles is %f%
echo Optimization for Fortran compiles is %o%
echo Command to build NONMEM archive is %ar%
echo Sizes is %s%
echo Recompile is %r% 
if  "%r%" == "rec" goto goodrec
if  "%r%" == "norec" goto goodrec
  echo The recompile option is not correct
  goto def
:goodrec

if "%r%" == "rec" (
echo All binaries will be recompiled
 ) else (
echo Binaries will not be recompiled, even if SIZES has changed.
 ) 
echo Interactive is %i%
rem choose version of unzip if not specified by user
echo Unzip is %u%
echo Encrypted source file is %zf%
echo Unencrypted file is %zu%
echo.
echo To learn how to override the defaults, enter
echo    SETUP73 help
echo.
set ans=y
if "%i%" == "i" set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto lexit

rem may not need to read files, but be ready to do so
if exist %cdd%\nonmem~1.0 set filepath=%cdd%\nonmem~1.0
if exist %cdd%\nonmem_7.3.0 set filepath=%cdd%\nonmem_7.3.0
if exist %cdd%\NONMEM_7.3.0 set filepath=%cdd%\NONMEM_7.3.0
rem make sure files can be read
  if exist %h% (
  echo directory  %h%
  )
if "%filepath%" == "" (
  echo Warning:
  echo Cannot find NONMEM_7.3.0 or nonmem_7.3.0 or nonmem~1.0 in the source path
  echo %cdd%
  if not exist %cdd% (
  echo No directory  %cdd%
  echo Installation Aborted
  goto lexit
  )
 ) 

set msgs=%h%\compilemsgs.txt
set emsgs=%h%\finishmsgs.txt
if not exist %h% (
echo The directory %h% does not exist.
echo Step 1. Creating %h%
mkdir %h%
if not exist %h% (
echo Cannot create %h%
goto lexit
 ) 
 ) 

echo Step 1a. Copy files from %cdd% to %h%
for %%n in (%cdd%\*.*) do if not exist %h%\%%~nxn copy %%n %h%\%%~nxn
%hdrv%
cd %h%
if not exist util mkdir util
for %%n in (%cdd%\util\*.*) do if not exist util\%%~nxn copy %%n util\%%~nxn

if exist %msgs% del %msgs%
if exist %emsgs% del %emsgs%

echo Step 1b. Copy files from  %filepath% to %h%
if "%filepath%" == "" goto skipcopy
for %%n in (%filepath%\*.*) do if not exist %h%\%%~nxn copy %%n %h%\%%~nxn >%temp%\trash.txt

:skipcopy
%hdrv%
cd %h%

echo.
echo Installing NONMEM 7.3.0 (Enter Ctrl-C to interrupt) ...

date /t >SETUP.TXT
time /t >>SETUP.TXT

echo The command typed: >> SETUP.TXT
echo SETUP73 %comline% >> SETUP.TXT
echo The effective command (with defaults): >> SETUP.TXT
echo SETUP73 %cdd% %h% %f% %o% %ar% %s% %r% %i% %u% %zf% %zu% >> SETUP.TXT
echo. >> SETUP.TXT

if not exist %h%\%zf% (
echo %zf% is not in directory %h%
goto end2
 ) 


if exist nm (
echo Directories already exist - skipping step 2a.
%hdrv%
cd %h%
goto step2
 ) 
echo Step 2a. Make sub-directories of %h%
mkdir nm nm\imsl pr tr resource source run

:step2
rem extract rest of files if not done already
if not exist resource\SIZES.f90 (
echo Extract rest of files
%u% -q -a %h%\%zu%
if not exist resource\SIZES.f90 (
echo %u% %h%\%zu% failed
goto fail
 ) 
rename run runfiles
 ) 
:step3
echo Using resource\SIZES.f90

:step4
rem always choose install binary and finish script 
set os=Win
set cpu=
set inst=install_%os%.exe
echo Choosing platform-dependent files ...

echo Will use %inst%
copy %inst% nminstl.exe >%temp%\trash.txt
rem comment out next two copy lines when new versions are incorporated into nonmem73r.zip
rem copy finish_win_ifort.bat util
rem copy finish_win_gfortran.bat util
if "%r" == "rec" (
if exist finish.bat (
 echo Will use existing finish.bat
 goto havefinish
)
)
set fin=util\finish_win_ifort.bat
if "%f%"=="gfortran" set fin=util\finish_win_gfortran.bat
if "%f%"=="g95" set fin=util\finish_win_gfortran.bat
echo Will use %fin%
copy %fin% finish.bat >%temp%\trash.txt
:havefinish

set incdir=/include:resource
if "%f%"=="gfortran" set incdir=-Iresource
if "%f%"=="g95" set incdir=-Iresource

set mod=/module:resource
if "%f%"=="gfortran" set mod=-Jresource
if "%f%"=="g95" set mod=-fmod=resource

rem set options for compile
set op=
set oext=obj
set lext=lib
if "%f%" == "gfortran" set oext=o
if "%f%" == "g95" set oext=o
if "%f%" == "gfortran" set lext=a
if "%f%" == "g95" set lext=a
set opc=/c
if "%f%" == "gfortran" set opc=-c
if "%f%" == "g95" set opc=-c
if "%f%" == "ifort" set op=/nologo /Od
if "%o%" == "y" (
if "%f%" == "ifort" set op=/Gs /nologo /nbs /w /fp:strict
if "%f%" == "df" set op=/nologo /nowarn /fast /noinline /4Yportlib /traceback
if "%f%" == "gfortran" set op=-O3 -ffast-math
if "%f%" == "g95" set op=-O3 -ffast-math
)

rem Now test that compiler and install can be used
echo.
echo ===Checking compiler
call .\util\dotest.bat %f%
echo ===
if errorlevel 1 goto fail0

rem options for nmfe73
set rop=%op%

set pipe=n
if "%f%" == "g95" set pipe=y

if "%f%" == "gfortran" (
set pipe=y
set op=%op% -xf95
 ) 

if "%f%" == "gf95" (
set pipe=y
set op=%op% -xf95
 ) 

:step5

if not exist license mkdir license
if exist license\license.lic (
 echo A NONMEM license file exists:
 echo %h%\license\nonmem.lic
) else (
 echo Copying NONMEM license file nonmem.lic to 
 echo %h%\license\nonmem.lic
copy %h%\nonmem.lic license >%temp%\trash.txt
)
 echo Warning: the license may be expired or may expire soon.
 echo When this happens, NONMEM runs will fail.
 echo If you have a new license file from IDS, you may stop, replace 
 echo it and restart SETUP73. Otherwise, allow NONMEM installation 
 echo to finish at this time.
 echo Obtain a new license file from IDS (IDSSOFTWARE@iconplc.com)
 echo as soon as possible and replace
 echo %h%\license\nonmem.lic
echo.

if not "%i%" == "i" goto skipi1
set ans=y
echo Changes to License file, resource\SIZES.f90 and other resource 
echo files may be made here.
echo.
set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto lexit
:skipi1

rem check if SIZES is newer than the important object and executables
set warn=n

if "%r" == "rec" goto skipnewer

.\util\doifnewer resource\SIZES.f90 resource\SIZES.%oext% 
if errorlevel 5 echo resource\SIZES.f90 is newer than resource\SIZES.%oext%
if errorlevel 5 set warn=y
.\util\doifnewer resource\SIZES.f90 nm\nonmem.%lext% 
if errorlevel 5 echo resource\SIZES.f90 is newer than nm\nonmem.%lext%
if errorlevel 5 set warn=y
.\util\doifnewer resource\SIZES.f90 pr\ZSPOW2.%oext% 
if errorlevel 5 echo resource\SIZES.f90 is newer than pr\ZSPOW2.%oext%
if errorlevel 5 set warn=y
.\util\doifnewer resource\SIZES.f90 tr\NMTRAN.exe
if errorlevel 5 echo resource\SIZES.f90 is newer than tr\NMTRAN.exe
if errorlevel 5 set warn=y

:skipnewer

if not "%warn%" == "y" goto step6
if not "%r%" == "norec" goto step6
echo WARNING: You should rerun SETUP73 with recompile set to rec, e.g.,
echo SETUP73 %cdd% %h% %f% %o% %ar% %s% rec

if not "%i%" == "i" goto skipi2
echo.

set ans=y
set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto lexit

:skipi2
if "%warn%" == "y" (
if "%r%" == "norec" (
echo All binaries should be recompiled.
 )
 )

:step6
set sizesfile=resource\SIZES.%oext%

if exist %sizesfile% (
if exist resource\sizes.mod (
  if "%r%" == "norec" (
  echo resource files were compiled - skipping recompile
  goto step7
   ) 
 ) 
)

echo Step 6.  Compile resource files
.\nminstl.exe 2 %zf% %zu% %f% "%opc% %op% %incdir%" "%mod%" Win "%u% -a" %oext% %pipe% x  >> %msgs% 2>&1

if exist resource\resource.%lext% del resource\resource.%lext%

if not exist %sizesfile% (
 echo Compile of resource files failed.
 goto fail
 ) 
if not exist resource\sizes.mod (
 echo Compile of resource files failed.
 goto fail
 ) 

echo Compile of resource files was successful.



:step7
 if exist resource\resource.%lext% goto step8
 cd resource

rem if "%f%"=="gfortran"  %ar% -r resource.%lext% *.%oext%
rem if "%f%"=="g95" %ar% -r resource.%lext% *.%oext%
if "%f%"=="gfortran" for %%b in (*.%oext%) do %ar% -r resource.%lext% %%b
if "%f%"=="g95" for %%b in (*.%oext%) do %ar% -r resource.%lext% %%b
if "%f%"=="ifort" %ar% /lib /out:resource.%lext% *.%oext%
if "%f%"=="df" %ar% /lib /out:resource.%lext% *.%oext%

 if not exist resource.%lext% (
  echo %ar% could not create resource.%lext% 
  goto fail
  ) 
 cd ..


:step8
echo resource directory has been compiled
set ans=y
if "%i%" == "i" set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto lexit


:step9
if not exist nm\ZXMIN2.%oext% goto skipto9
if not exist pr\ZSPOW2.%oext% goto skipto9
if not exist tr\XABBR.%oext% goto skipto9
 if "%r%" == "norec" (
 echo object files exist - skipping step 9.
 goto step10
 ) 

:skipto9
echo.
echo Step 9.  Compile all remaining source files
echo This will take a long time.
echo See file %h%\progress.txt for compiler progress
.\nminstl.exe 3 %zf% %zu% %f% "%opc% %op% %incdir%" "%mod%" Win  "%u% -a" %oext% %pipe% x  >> %msgs% 2>&1

if not exist nm\ZXMIN2.%oext% goto fail9
if not exist pr\ZSPOW2.%oext% goto fail9
if not exist tr\XABBR.%oext% fail9
goto step10
:fail9
 echo Compile of source files failed.
 goto fail

:step10
if exist nm\nonmem.%lext% (
if "%r%" == "norec" (
 echo NONMEM archive nm\nonmem.%lext% exists - skipping step 10.
 goto step13
 )
) 

echo Step 10. Build NONMEM archive nm\nonmem.%lext%
if exist %h%\nm\termiolin.o del %h%\nm\termiolin.o >%temp%\trash.txt
call finish.bat %f% >>  %emsgs% 2>&1

if not exist nm\nonmem.%lext% (
  echo nonmem.%lext% was not created
  goto failc
 ) 
if not exist tr\NMTRAN.exe (
  echo NMTRAN.exe was not created
  goto failc
 ) 

:step13
if exist util\nmfe73.bat (
if "%r%" == "norec" (
echo nmfe73 batch file util\nmfe73.bat exists - skipping step 13.
goto step14
 ) 
 )

echo Step 13. Create shell script util\nmfe73
rem the following copy lines are temporary until they are incoroproated in nonmem73r.zip
rem copy nmfe73original.bat util
rem copy nmfe73goriginal.bat util

cd util
echo @echo off> nmfe73.bat
echo set dir=%h%>> nmfe73.bat
echo set f=%f%>> nmfe73.bat
echo set op=%rop%>> nmfe73.bat
type nmfe73original.bat >> nmfe73.bat

if exist ..\run\nmfe73.bat (
if "%r%" == "rec" copy nmfe73.bat ..\run >%temp%\trash.txt
if "%r%" == "rec" copy nmfe73.bat ..\run\nmfePDx.bat >%temp%\trash.txt
)
cd ..

:step14
if exist run (
echo Directory run exists - skipping step 14.
goto done
 ) 
echo.
echo Step 14. Create directory run
mkdir run
copy util\nmfe73.bat run >%temp%\trash.txt
copy util\nmfe73.bat run\nmfePDx.bat >%temp%\trash.txt
copy util\CONTROL3 run >%temp%\trash.txt
copy util\CONTROL5 run >%temp%\trash.txt
copy util\DATA3 run >%temp%\trash.txt
copy util\THEOPP run >%temp%\trash.txt
copy util\REPORT5.IDS run\REPORT5IDS.txt >%temp%\trash.txt
copy util\REPORT5IDS.txt run\REPORT5IDS.txt >%temp%\trash.txt
echo run contains the following files:
echo   nmfe73.bat CONTROL3 CONTROL5 DATA3 THEOPP REPORT5IDS.txt
copy runfiles\* run >trash.out
echo run contains additional files

:done
copy SETUP73 util >%temp%\trash.txt
copy SETUP.TXT util >%temp%\trash.txt
if exist listall del listall
if exist listallread del listallread
if exist listres del listres
if exist listresread del listresread
echo. 
echo Installation of NONMEM 7.3.0 is finished.
echo For future reference, the command has been saved in file SETUP.TXT
echo.
echo Please review %msgs%
echo               %emsgs%
echo Ignore warnings. Report error messages.
echo. 

:step15
set ans=y
if not "%i%" == "i" goto skipi3
echo Do you want to install help and html files and Users Guides?
set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto step20
:skipi3

echo Help and html files and Users Guides will be installed.

rem might have been read upper or lower case.
if exist unzipmsgs.txt del unzipmsgs.txt
if not exist guides\I.pdf goto do15
echo NONMEM Users Guide files exist - skipping step 15.
goto step16

:do15
echo Step 15. Install NONMEM Users Guide files in %h%\guides
if not exist guides.zip (
  echo Cannot find guides.zip
  goto step17a
 ) 
if not exist guides mkdir guides
echo Unzipping guides.zip...
%u% -q -a guides.zip  >>unzipmsgs.txt 2>&1

:step16
echo To use the NONMEM Users Guides,
echo  open pdf files in %h%\guides using Adobe Reader.

:step17a
if not exist help\index goto do16
echo On-line help files exist - skipping step 16
goto step18

:do16
echo Step 16. Install On-line help files in %h%\help
if not exist help.zip (
  echo Cannot find help.zip
  goto do17
 ) 
if not exist help mkdir help
echo Unzipping help.zip...
%u% -q -a help.zip  >>unzipmsgs.txt 2>&1

echo "Installing On-Line Help Tools ..."
%u% -q -o -d help help\helptools.zip  >>unzipmsgs.txt 2>&1

:step17
echo For online help, enter (e.g.)
echo     cd %h%\help
echo     nmhelp advan2
if not exist html\index.htm goto do17
echo On-line HTML files exist - skipping step 16.
goto step18

:do17
echo Step 17. Install HTML files in %h%\html (this may take a while) ...
if not exist html.zip (
  echo Cannot find html.zip
  goto step20
 ) 
if not exist html mkdir html
echo Unzipping html.zip...
%u% -q -o -a html.zip  >>unzipmsgs.txt 2>&1

:step18
echo For online help HTML files,
echo   open %h%\html\index.htm in web browser

:step20
echo.
set ans=y
if not "%i%"== "i" goto skipi4
echo Do you want to test the installation?
set /p ans=Continue (y/n)? [y]
if "%ans%" == "n" goto end2
:skipi4
echo Testing the installation. Commands are
echo     cd %h%\run
echo     nmfe73 CONTROL5 REPORT5.txt -prdefault
echo If the run is successful, file REPORT5.txt will be created.
echo. 
%hdrv%
cd %h%\run
if exist OUTPUT del OUTPUT
set fold=%f%
call nmfe73 CONTROL5 REPORT5.txt -prdefault
set f=%fold%

if not exist nonmem.exe goto end2
rem find /i "#OBJV:" REPORT5.txt >%temp%\trash.txt 2>&1
find /i "#OBJV:" REPORT5.txt >trash.txt 2>&1
if errorlevel 1 (
 echo Nonmem executable was created but did not run successfully.
 echo This may be because the license is expired.
 echo "Obtain a new license file from IDS (IDSSOFTWARE@iconplc.com)"
 echo and replace
 echo %h%\license\nonmem.lic
 goto lexit
)

echo. 
echo You should now compare REPORT5.txt vs. REPORT5IDS.txt
echo Values should be similar.
echo E.g., the following should be identical:
echo find  "#OBJV:" REPORT5.txt
find  "#OBJV:" REPORT5.txt
echo find  "#OBJV:" REPORT5IDS.txt
find  "#OBJV:" REPORT5IDS.txt
if not exist ..\nmloc.bat (
cd ..\util
getcompiler.exe %f%
copy nmloc.bat ..\nmloc.bat
)
cd ..
:end2
goto lexit

:fail0
echo. 
echo Compiler %f% and/or install program cannot be run
echo Installation Aborted
goto lexit
:fail
echo. 
set pwd=%cd%
echo Compilation failed.
echo Please look for error messages in %msgs%
echo Installation Aborted
goto lexit
:failc
echo. 
set pwd=%cd%
echo Please look for error messages in %emsgs%
echo Installation Aborted
goto lexit
:lexit
