#!/usr/bin/perl
use strict;
unless(@ARGV){
	print "usage: perl sections2csv.pl section bootstrap_results.csv\n";
	die;
}
my $term = shift @ARGV;
my @file = <>;
my @out;
my $test='';
while (defined($test) & $test !~/$term/){
	$test= shift @file;
}
my $done=0;
while (@file && !$done){
	my $line = shift @file;
	if($line!~/^\w/){
		$line=~s/\"//g;
		$line=~s/\'//g;
		$line=~s/^\s+//g;
		$line=~s/, +/,/g;
		$line=~s/\((\d+),(\d+)\)/$1.$2/g;
		$line=~s/\(%\)/.percent/g;
		push @out, $line;
	}else{
		$done=1;
	}
}
print @out;
