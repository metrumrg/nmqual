<?xml version="1.0"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='text'/>
<xsl:template match='/'>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match='//contrast'>
Contrast Two XML Documents
<xsl:apply-templates select='aspect'/>
</xsl:template>

<xsl:template match='aspect'>
<xsl:text>
</xsl:text>
Search:<xsl:value-of select='path'/>
<xsl:text>
</xsl:text>
<xsl:choose>
<xsl:when test="count(left/val)!=count(right/val)">
Differing numbers of results!
</xsl:when>
<xsl:otherwise>
<xsl:if test="count(left/val) &gt; 0">
<xsl:call-template name='iterate'>
<xsl:with-param name='upto' select="count(left/val)"/>
</xsl:call-template>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name='iterate'>
<xsl:param name='index' select='1'/>
<xsl:param name='upto' select='1'/>
<xsl:call-template name='compare'>
<xsl:with-param name='left' select='left/val[$index]'/>
<xsl:with-param name='right' select='right/val[$index]'/>
</xsl:call-template>
<xsl:if test="$index &lt; $upto">
<xsl:call-template name='iterate'>
<xsl:with-param name='index' select='$index+1'/>
<xsl:with-param name='upto' select='$upto'/>
</xsl:call-template>
</xsl:if>
</xsl:template>

<xsl:template name='compare'>
 <xsl:param name='left'/>
 <xsl:param name='right'/>
 <xsl:choose>
  <xsl:when test="string(number($left)) = 'NaN'">
   <xsl:call-template name='compare-text'>
    <xsl:with-param name='left' select='$left'/>
    <xsl:with-param name='right' select='$right'/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <xsl:call-template name='compare-number'>
    <xsl:with-param name='left' select='$left'/>
    <xsl:with-param name='right' select='$right'/>
   </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name='compare-text'>
 <xsl:param name='left'/>
 <xsl:param name='right'/>
 <xsl:choose>
  <xsl:when test='$left=$right'>
   <xsl:text>.</xsl:text>
  </xsl:when>
  <xsl:otherwise>
<xsl:text>
</xsl:text>
<xsl:value-of select='$left'/>
<xsl:text>
</xsl:text>
<xsl:value-of select='$right'/>
<xsl:text>
</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name='compare-number'>
 <xsl:param name='left'/>
 <xsl:param name='right'/>
 <xsl:choose>
  <xsl:when test="string(number($left)) = '0'">
   <xsl:call-template name='compare-text'>
    <xsl:with-param name='left' select='$left'/>
    <xsl:with-param name='right' select='$right'/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <xsl:variable name='ratio' select='$right div $left'/>
   <xsl:choose>
    <xsl:when test='$ratio >= 0.99 and $ratio &lt;= 1.01'>
     <xsl:call-template name='compare-text'>
      <xsl:with-param name='left' select='$left'/>
      <xsl:with-param name='right' select='$left'/>
     </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
     <xsl:call-template name='compare-text'>
      <xsl:with-param name='left' select='$left'/>
      <xsl:with-param name='right' select='$right'/>
     </xsl:call-template>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:transform>