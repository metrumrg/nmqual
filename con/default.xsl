<?xml version="1.0"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='text'/>
<xsl:template match='/'>
<xsl:apply-templates/>
</xsl:template>

<xsl:template match='//contrast'>
Contrast Two XML Documents
<xsl:apply-templates select='aspect'/>
</xsl:template>

<xsl:template match='aspect'>
Search:<xsl:value-of select='path'/>
<xsl:choose>
<xsl:when test="count(left/val)!=count(right/val)">
Differing numbers of results!
</xsl:when>
<xsl:otherwise>
<xsl:if test="count(left/val) &gt; 0">
<xsl:call-template name='iterate'>
<xsl:with-param name='upto' select="count(left/val)"/>
</xsl:call-template>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name='iterate'>
<xsl:param name='index' select='1'/>
<xsl:param name='upto' select='1'/>
<xsl:text>
</xsl:text>
<xsl:value-of select="left/val[$index]"/>
<xsl:text>
</xsl:text>
<xsl:value-of select="right/val[$index]"/>
<xsl:text>
</xsl:text>
<xsl:if test="$index &lt; $upto">
<xsl:call-template name='iterate'>
<xsl:with-param name='index' select='$index+1'/>
<xsl:with-param name='upto' select='$upto'/>
</xsl:call-template>
</xsl:if>
</xsl:template>

</xsl:transform>